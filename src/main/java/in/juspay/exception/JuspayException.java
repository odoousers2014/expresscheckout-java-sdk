package in.juspay.exception;

public abstract class JuspayException extends Exception {
    int httpResponseCode;
    String status;
    String errorCode;
    String errorMessage;

    public JuspayException(int httpResponseCode, String status, String errorCode, String errorMessage) {
        super(errorMessage);
        this.httpResponseCode = httpResponseCode;
        this.status = status;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    public String getStatus() {
        return status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "JuspayException{" +
                "httpResponseCode=" + httpResponseCode +
                ", status='" + status + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
