package in.juspay.model;

import com.google.gson.*;
import in.juspay.exception.APIConnectionException;
import in.juspay.exception.APIException;
import in.juspay.exception.AuthenticationException;
import in.juspay.exception.InvalidRequestException;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MapMessage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class JuspayEntity {

    private static final Logger log = LogManager.getLogger(JuspayEntity.class);

    private static String serializeParams(Map<String, Object> params) {
        if (params == null || params.size() == 0) {
            return "";
        }
        StringBuilder serializedParams = new StringBuilder();
        try {
            for (String key : params.keySet()) {
                serializedParams.append(key + "=");
                if (params.get(key) != null) {
                    serializedParams.append(URLEncoder.encode(params.get(key).toString(), "UTF-8"));
                }
                serializedParams.append("&");
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding exception while trying to construct payload", e);
        }
        if (serializedParams.charAt(serializedParams.length() - 1) == '&') {
            serializedParams.deleteCharAt(serializedParams.length() - 1);
        }
        return serializedParams.toString();
    }

    protected static JsonObject makeServiceCall(String path, Map<String, Object> params, RequestMethod method, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (requestOptions == null) {
            requestOptions = RequestOptions.createDefault();
        }
        CloseableHttpClient httpclient = null;
        if (JuspayEnvironment.getBaseUrl() == JuspayEnvironment.DEVELOPMENT_BASE_URL) {
            SSLContextBuilder builder = new SSLContextBuilder();
            SSLConnectionSocketFactory sslsf = null;
            try {
                builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
                sslsf = new SSLConnectionSocketFactory(builder.build(), NoopHostnameVerifier.INSTANCE);
            } catch (Exception e) {
            }
            httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
        } else {
            httpclient = HttpClients.createDefault();
        }
        String encodedKey = new String(Base64.encodeBase64(requestOptions.getApiKey().getBytes())).replaceAll("\n", "");
        try {
            MapMessage mapMessage = new MapMessage();
            if (params != null) {
                for (String key : params.keySet()) {
                    String value = "";
                    if (params.get(key) != null) {
                        value = params.get(key).toString();
                    }
                    mapMessage.put(key, value);
                }
            }
            String url = JuspayEnvironment.getBaseUrl() + path;
            log.info("Executing request: " + method + " " + url);
            log.info("Request parameters: ");
            // Printing this map separately to allow CardNumber and CVV filtering.
            log.info(mapMessage);
            CloseableHttpResponse response;
            if (method == RequestMethod.GET) {
                String encodedParams = serializeParams(params);
                if (encodedParams != null && !encodedParams.equals("")) {
                    url += "?" + encodedParams;
                }
                HttpGet httpGet = new HttpGet(url);
                httpGet.setHeader("Authorization", String.format("Basic %s", encodedKey));
                httpGet.setHeader("version", JuspayEnvironment.API_VERSION);
                httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
                httpGet.setHeader("Content-Language", "en-US");
                httpGet.setHeader("User-Agent", JuspayEnvironment.SDK_VERSION);
                response = httpclient.execute(httpGet);
            } else {
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Authorization", String.format("Basic %s", encodedKey));
                httpPost.setHeader("version", JuspayEnvironment.API_VERSION);
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                httpPost.setHeader("Content-Language", "en-US");
                httpPost.setHeader("User-Agent", JuspayEnvironment.SDK_VERSION);
                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                for (String key : params.keySet()) {
                    if (params.get(key) != null) {
                        nvps.add(new BasicNameValuePair(key, params.get(key).toString()));
                    } else {
                        nvps.add(new BasicNameValuePair(key, ""));
                    }
                }
                httpPost.setEntity(new UrlEncodedFormEntity(nvps));
                response = httpclient.execute(httpPost);
            }

            int httpResponseCode = response.getStatusLine().getStatusCode();
            String responseString = null;
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                responseString = EntityUtils.toString(entity);
            }
            log.info("Received HTTP Response Code: " + httpResponseCode);
            log.info("Received response: " + responseString);
            JsonObject resJson = null;
            try {
                resJson = responseString != null ? new JsonParser().parse(responseString).getAsJsonObject() : null;
            } catch (JsonSyntaxException e) {
                // Do nothing, resJson will remain null.
            }
            if (httpResponseCode >= 200 && httpResponseCode < 300) {
                return resJson;
            } else {
                String status = null;
                String errorCode = null;
                String errorMessage = null;
                if (resJson != null) {
                    if (resJson.has("status")) {
                        status = resJson.get("status").getAsString();
                    }
                    if (resJson.has("error_code")) {
                        errorCode = resJson.get("error_code").getAsString();
                    }
                    if (resJson.has("error_message")) {
                        errorMessage = resJson.get("error_message").getAsString();
                    }
                }
                switch (httpResponseCode) {
                    case 400:
                    case 404:
                        throw new InvalidRequestException(httpResponseCode, status, errorCode, errorMessage);
                    case 401:
                        throw new AuthenticationException(httpResponseCode, status, errorCode, errorMessage);
                    default:
                        throw new APIException(httpResponseCode, "internal_error", "internal_error", "Something went wrong.");
                }
            }
        } catch (IOException e) {
            throw new APIConnectionException(-1, "connection_error", "connection_error", e.getMessage());
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
                throw new APIConnectionException(-1, "connection_error", "connection_error", e.getMessage());
            }
        }
    }

    protected static JsonObject addInputParamsToResponse(Map<String, Object> params, JsonObject response) {
        JsonObject inputJson = new JsonParser().parse(new GsonBuilder().create().toJson(params)).getAsJsonObject();
        Iterator<Map.Entry<String, JsonElement>> iterator = inputJson.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, JsonElement> entry = iterator.next();
            response.add(entry.getKey(), entry.getValue());
        }
        return response;
    }

    protected static <T> T createEntityFromResponse(JsonElement response, Class<T> entityClass) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        T entity = gson.fromJson(response, entityClass);
        return entity;
    }

    protected enum RequestMethod {
        GET, POST
    }

}