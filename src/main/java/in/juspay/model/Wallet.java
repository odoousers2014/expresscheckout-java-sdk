package in.juspay.model;

import com.google.gson.JsonObject;
import in.juspay.exception.APIConnectionException;
import in.juspay.exception.APIException;
import in.juspay.exception.AuthenticationException;
import in.juspay.exception.InvalidRequestException;

import java.util.Date;

public class Wallet extends JuspayEntity {

    private String id;
    private String object;
    private String wallet;
    private String token;
    private Double currentBalance;
    private Date lastRefreshed;

    public static WalletList list(String customerId)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return list(customerId, null);
    }

    public static WalletList list(String customerId, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (customerId == null || customerId.equals("")) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/customers/" + customerId + "/wallets", null, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, WalletList.class);
    }

    public static WalletList refresh(String customerId)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return refresh(customerId, null);
    }

    public static WalletList refresh(String customerId, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (customerId == null || customerId.equals("")) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/customers/" + customerId + "/wallets/refresh-balances", null, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, WalletList.class);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Date getLastRefreshed() {
        return lastRefreshed;
    }

    public void setLastRefreshed(Date lastRefreshed) {
        this.lastRefreshed = lastRefreshed;
    }

}
