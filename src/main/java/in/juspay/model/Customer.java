package in.juspay.model;

import com.google.gson.JsonObject;
import in.juspay.exception.APIConnectionException;
import in.juspay.exception.APIException;
import in.juspay.exception.AuthenticationException;
import in.juspay.exception.InvalidRequestException;

import java.util.Date;
import java.util.Map;

public class Customer extends JuspayEntity {

    private String id;
    private String object;
    private String firstName;
    private String lastName;
    private String mobileCountryCode;
    private String mobileNumber;
    private String emailAddress;
    private Date dateCreated;
    private Date lastUpdated;
    private String objectReferenceId;

    public static Customer create(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return create(params, null);
    }

    public static Customer create(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/customers", params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Customer.class);
    }

    public static Customer update(String id, Map<String, Object> params)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return update(id, params, null);
    }

    public static Customer update(String id, Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (id == null || id.equals("") || params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/customers/" + id, params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Customer.class);
    }

    public static CustomerList list(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return list(params, null);
    }

    public static CustomerList list(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        JsonObject response = makeServiceCall("/customers", params, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, CustomerList.class);
    }

    public static Customer get(String id)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return get(id, null);
    }

    public static Customer get(String id, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (id == null || id.equals("")) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/customers/" + id, null, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, Customer.class);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(String mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getObjectReferenceId() {
        return objectReferenceId;
    }

    public void setObjectReferenceId(String objectReferenceId) {
        this.objectReferenceId = objectReferenceId;
    }

}
