package in.juspay.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import in.juspay.exception.APIConnectionException;
import in.juspay.exception.APIException;
import in.juspay.exception.AuthenticationException;
import in.juspay.exception.InvalidRequestException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Card extends JuspayEntity {

    private String cardNumber;
    private String nameOnCard;
    private String cardExpYear;
    private String cardExpMonth;
    private String cardSecurityCode;
    private String nickname;
    private String cardToken;
    private String cardReference;
    private String cardFingerprint;
    private String cardIsin;
    private String lastFourDigits;
    private String cardType;
    private String cardIssuer;
    private Boolean savedToLocker;
    private Boolean expired;
    private String cardBrand;

    public static Card create(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return create(params, null);
    }

    public static Card create(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/card/add", params, RequestMethod.POST, requestOptions);
        response = addInputParamsToResponse(params, response);
        return createEntityFromResponse(response, Card.class);
    }

    public static List<Card> list(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return list(params, null);
    }

    public static List<Card> list(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/card/list", params, RequestMethod.GET, requestOptions);
        List<Card> cardList = new ArrayList<Card>();
        if (response.has("cards")) {
            JsonArray cardArray = response.get("cards").getAsJsonArray();
            for (int i = 0; i < cardArray.size(); i++) {
                cardList.add(createEntityFromResponse(cardArray.get(i), Card.class));
            }
        }
        return cardList;
    }

    public static boolean delete(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return delete(params, null);
    }

    public static boolean delete(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/card/delete", params, RequestMethod.POST, requestOptions);
        return response.get("deleted").getAsBoolean();
    }

    public Boolean getSavedToLocker() {
        return savedToLocker;
    }

    public void setSavedToLocker(Boolean savedToLocker) {
        this.savedToLocker = savedToLocker;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getCardExpYear() {
        return cardExpYear;
    }

    public void setCardExpYear(String cardExpYear) {
        this.cardExpYear = cardExpYear;
    }

    public String getCardExpMonth() {
        return cardExpMonth;
    }

    public void setCardExpMonth(String cardExpMonth) {
        this.cardExpMonth = cardExpMonth;
    }

    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    public void setCardSecurityCode(String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getCardReference() {
        return cardReference;
    }

    public void setCardReference(String cardReference) {
        this.cardReference = cardReference;
    }

    public String getCardFingerprint() {
        return cardFingerprint;
    }

    public void setCardFingerprint(String cardFingerprint) {
        this.cardFingerprint = cardFingerprint;
    }

    public String getCardIsin() {
        return cardIsin;
    }

    public void setCardIsin(String cardIsin) {
        this.cardIsin = cardIsin;
    }

    public String getLastFourDigits() {
        return lastFourDigits;
    }

    public void setLastFourDigits(String lastFourDigits) {
        this.lastFourDigits = lastFourDigits;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardIssuer() {
        return cardIssuer;
    }

    public void setCardIssuer(String cardIssuer) {
        this.cardIssuer = cardIssuer;
    }

    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }
}
