package in.juspay.model;

import java.util.List;

public abstract class JuspayEntityList<T extends JuspayEntity> {
    List<T> list;
    long count;
    long offset;
    long total;
}
