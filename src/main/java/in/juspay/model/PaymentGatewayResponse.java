package in.juspay.model;

import java.util.Date;

public class PaymentGatewayResponse extends JuspayEntity {
    String rrn;
    String epgTxnId;
    String authIdCode;
    String txnId;
    String respCode;
    String respMessage;
    Date created;

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getEpgTxnId() {
        return epgTxnId;
    }

    public void setEpgTxnId(String epgTxnId) {
        this.epgTxnId = epgTxnId;
    }

    public String getAuthIdCode() {
        return authIdCode;
    }

    public void setAuthIdCode(String authIdCode) {
        this.authIdCode = authIdCode;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
