package in.juspay.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import in.juspay.exception.APIConnectionException;
import in.juspay.exception.APIException;
import in.juspay.exception.AuthenticationException;
import in.juspay.exception.InvalidRequestException;

import java.util.Iterator;
import java.util.Map;

public class Payment extends JuspayEntity {

    String orderId;
    String txnId;
    String status;
    String method;
    String url;
    Map<String, String> params;

    public static Payment create(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        return create(params, null);
    }

    public static Payment create(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        // We will always send it as json in SDK.
        params.put("format", "json");
        JsonObject response = makeServiceCall("/txns", params, RequestMethod.POST, requestOptions);
        response = updatePaymentResponseStructure(response);
        return createEntityFromResponse(response, Payment.class);
    }

    // Restructuring the payment response. Removed unnecessary hierarchy in the response.
    private static JsonObject updatePaymentResponseStructure(JsonObject response) {
        JsonObject authResp = response.get("payment").getAsJsonObject().get("authentication").getAsJsonObject();
        response.add("method", authResp.get("method"));
        response.add("url", authResp.get("url"));
        if (response.get("method").getAsString().equals("POST")) {
            response.add("params", new JsonObject());
            Iterator<Map.Entry<String, JsonElement>> iterator = authResp.get("params").getAsJsonObject().entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, JsonElement> entry = iterator.next();
                response.get("params").getAsJsonObject().add(entry.getKey(), entry.getValue());
            }
        }
        response.remove("payment");
        return response;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

}
