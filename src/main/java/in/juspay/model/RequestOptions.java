package in.juspay.model;

public class RequestOptions {
    private String apiKey;

    private RequestOptions() {
        apiKey = JuspayEnvironment.getApiKey();
    }

    public static RequestOptions createDefault() {
        return new RequestOptions();
    }

    public String getApiKey() {
        return apiKey;
    }

    public RequestOptions withApiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

}
