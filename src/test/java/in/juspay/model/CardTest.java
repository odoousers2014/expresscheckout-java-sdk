package in.juspay.model;

import in.juspay.exception.JuspayException;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CardTest {

    String customerId;
    Card card;

    @Before
    public void setUp() {
        TestEnvironment.setUp();
    }

    @Test
    public void testCreate() throws JuspayException {
        customerId = UUID.randomUUID().toString().substring(0, 12);
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("merchant_id", TestEnvironment.merchantId);
        params.put("customer_id", customerId);
        params.put("customer_email", "support@juspay.in");
        params.put("card_number", "4111111111111111");
        params.put("card_exp_year", "2018");
        params.put("card_exp_month", "07");
        params.put("name_on_card", "Juspay Technologies");
        params.put("nickname", "ICICI VISA");
        Card card = Card.create(params);
        assertNotNull(card);
        assertNotNull(card.getCardToken());
        assertNotNull(card.getCardReference());
        assertNotNull(card.getCardFingerprint());
        this.card = card;
    }

    @Test
    public void testList() throws JuspayException {
        testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("customer_id", customerId);
        List<Card> cards = Card.list(params);
        assertNotNull(cards);
        assertEquals(1, cards.size());
    }

    @Test
    public void testDelete() throws JuspayException {
        testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("card_token", this.card.getCardToken());
        boolean deleted = Card.delete(params);
        assertEquals(true, deleted);
    }
}