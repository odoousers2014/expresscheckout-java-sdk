package in.juspay.model;

import in.juspay.exception.JuspayException;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.*;

public class CustomerTest {
    Customer customer;

    @Before
    public void setUp() {
        TestEnvironment.setUp();
    }

    @Test
    public void testCreate() throws JuspayException {
        String customerId = UUID.randomUUID().toString().substring(0, 12);
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("first_name", "Juspay");
        params.put("last_name", "Technologies");
        params.put("mobile_country_code", "91");
        params.put("mobile_number", "9988776655");
        params.put("email_address", "support@juspay.in");
        params.put("object_reference_id", customerId);
        Customer customer = Customer.create(params);
        assertNotNull(customer);
        assertNotNull(customer.getId());
        assertEquals(params.get("first_name"), customer.getFirstName());
        assertEquals(params.get("last_name"), customer.getLastName());
        assertEquals(params.get("mobile_country_code"), customer.getMobileCountryCode());
        assertEquals(params.get("mobile_number"), customer.getMobileNumber());
        assertEquals(params.get("email_address"), customer.getEmailAddress());
        assertEquals(params.get("object_reference_id"), customer.getObjectReferenceId());
        this.customer = customer;
    }

    @Test
    public void testGet() throws JuspayException {
        testCreate();
        Customer customer = Customer.get(this.customer.getId());
        assertNotNull(customer);
    }

    @Test
    public void testList() throws JuspayException {
        testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        CustomerList customers = Customer.list(params);
        assertNotNull(customers);
        assertNotEquals(0, customers.list.size());
    }

    @Test
    public void testUpdate() throws JuspayException {
        testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("first_name", "Juspay1");
        params.put("last_name", "Technologies1");
        params.put("mobile_country_code", "92");
        params.put("mobile_number", "9988776656");
        params.put("email_address", "support1@juspay.in");
        Customer customer = Customer.update(this.customer.getId(), params);
        assertNotNull(customer);
        assertNotNull(customer.getId());
        assertEquals(params.get("first_name"), customer.getFirstName());
        assertEquals(params.get("last_name"), customer.getLastName());
        assertEquals(params.get("mobile_country_code"), customer.getMobileCountryCode());
        assertEquals(params.get("mobile_number"), customer.getMobileNumber());
        assertEquals(params.get("email_address"), customer.getEmailAddress());
    }

}
