package in.juspay.model;

import in.juspay.exception.JuspayException;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.*;

public class OrderTest {
    Order order;

    @Before
    public void setUp() {
        TestEnvironment.setUp();
    }

    @Test
    public void testCreate() throws JuspayException {
        String orderId = UUID.randomUUID().toString().substring(0, 12);
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("order_id", orderId);
        params.put("amount", 10000.0);
        params.put("currency", "INR");
        params.put("customer_id", "juspay_test_1");
        params.put("customer_email", "test@juspay.in");
        params.put("customer_phone", "9988776655");
        params.put("product_id", "123456");
        params.put("return_url", "https://abc.xyz.com/123456");
        params.put("description", "Sample Description");
        params.put("billing_address_first_name", "Juspay");
        params.put("billing_address_last_name", "Technologies");
        params.put("billing_address_line1", "Girija Building");
        params.put("billing_address_line2", "Ganapathi Temple Road");
        params.put("billing_address_line3", "8th Block, Koramangala");
        params.put("billing_address_city", "Bengaluru");
        params.put("billing_address_state", "Karnataka");
        params.put("billing_address_country", "India");
        params.put("billing_address_postal_code", "560095");
        params.put("billing_address_phone", "9988776655");
        params.put("billing_address_country_code_iso", "IND");
        params.put("shipping_address_first_name", "Juspay");
        params.put("shipping_address_last_name", "Technologies");
        params.put("shipping_address_line1", "Girija Building");
        params.put("shipping_address_line2", "Ganapathi Temple Road");
        params.put("shipping_address_line3", "8th Block, Koramangala");
        params.put("shipping_address_city", "Bengaluru");
        params.put("shipping_address_state", "Karnataka");
        params.put("shipping_address_country", "India");
        params.put("shipping_address_postal_code", "560095");
        params.put("shipping_address_phone", "9988776655");
        params.put("shipping_address_country_code_iso", "IND");
        Order order = Order.create(params);
        assertNotNull(order);
        assertEquals("CREATED", order.getStatus());
        assertEquals(1, order.getStatusId().longValue());
        this.order = order;
    }

    @Test
    public void testStatus() throws JuspayException {
        testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("order_id", this.order.getOrderId());
        Order order = Order.status(params);
        assertNotNull(order);
        assertEquals(this.order.getOrderId(), order.getOrderId());
    }

    @Test
    public void testList() throws JuspayException {
        testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        OrderList orderList = Order.list(params);
        assertNotNull(orderList);
        assertNotEquals(0, orderList.list.size());
    }

    @Test
    public void testUpdate() throws JuspayException {
        testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("order_id", this.order.getOrderId());
        params.put("amount", this.order.getAmount() + 100);
        Order order = Order.update(params);
        assertNotNull(order);
        assertEquals(order.getAmount().doubleValue(), this.order.getAmount().doubleValue() + 100, 0.00001);
    }

    @Test
    public void testRefund() throws JuspayException {
        testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("order_id", this.order.getOrderId());
        params.put("amount", 10);
        try {
            // Testing refund needs a successful order.
            // Here we are testing only unsuccessful orders using catch.
            Order order = Order.refund(params);
            assertNotNull(order);
            assertEquals(this.order.getOrderId(), order.getOrderId());
            assertEquals(true, order.getRefunded().booleanValue());
        } catch (JuspayException e) {
            assertEquals("invalid.order.not_successful", e.getErrorCode());
        }
    }

}
