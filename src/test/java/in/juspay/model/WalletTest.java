package in.juspay.model;

import in.juspay.exception.JuspayException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class WalletTest {

    CustomerTest customerTest;

    @Before
    public void setUp() {
        TestEnvironment.setUp();
    }

    @Test
    public void testList() throws JuspayException {
        customerTest = new CustomerTest();
        customerTest.testCreate();
        WalletList wallets = Wallet.list(customerTest.customer.getObjectReferenceId());
        assertNotNull(wallets);
    }

    @Test
    public void testRefresh() throws JuspayException {
        customerTest = new CustomerTest();
        customerTest.testCreate();
        WalletList wallets = Wallet.refresh(customerTest.customer.getObjectReferenceId());
        assertNotNull(wallets);
    }

}
