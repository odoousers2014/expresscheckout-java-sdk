package in.juspay.model;

import in.juspay.exception.JuspayException;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PaymentTest {

    @Before
    public void setUp() {
        TestEnvironment.setUp();
    }

    @Test
    public void testCreate() throws JuspayException {
        OrderTest orderTest = new OrderTest();
        orderTest.testCreate();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("order_id", orderTest.order.getOrderId());
        params.put("merchant_id", TestEnvironment.merchantId);
        params.put("payment_method_type", "CARD");
        params.put("payment_method", "MASTERCARD");
        params.put("redirect_after_payment", true);
        params.put("card_number", "5243681100075285");
        params.put("card_exp_month", "10");
        params.put("card_exp_year", "20");
        params.put("card_security_code", "111");
        params.put("save_to_locker", false);
        Payment payment = Payment.create(params);
        assertNotNull(payment);
        assertEquals(orderTest.order.getOrderId(), payment.orderId);
        assertEquals("PENDING_VBV", payment.status);
    }
}



