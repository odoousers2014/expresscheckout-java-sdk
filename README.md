The Java SDK for Juspay APIs.

REQUIREMENTS
------------

JDK 1.7 or higher.

INSTALLATION
------------

You can obtain the latest copy of the code using this command:

    git clone git@bitbucket.org:juspay/expresscheckout-java-sdk.git

Getting Started
---------------

For Maven, add the following dependency to your pom.xml file:

    <dependency>
      <groupId>in.juspay</groupId>
      <artifactId>expresscheckout</artifactId>
      <version>1.0.2</version>
    </dependency>

In other environments, manually install the following JARs:

1. ExpressCheckout-SDK from [expresscheckout-1.0.1.jar](https://bitbucket.org/juspay/expresscheckout-java-sdk/downloads/expresscheckout-1.0.1.jar).
2. Apache-HttpClient from [httpclient-4.5.2.jar](http://repo1.maven.org/maven2/org/apache/httpcomponents/httpclient/4.5.2/httpclient-4.5.2.jar).
3. Apache-HttpCore from [httpcore-4.4.4.jar](http://repo1.maven.org/maven2/org/apache/httpcomponents/httpcore/4.4.4/httpcore-4.4.4.jar).
4. Hamcrest-Core from [hamcrest-core-1.3.jar](http://repo1.maven.org/maven2/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar).
5. Commons-Codec from [commons-codec-1.9.jar](http://repo1.maven.org/maven2/commons-codec/commons-codec/1.9/commons-codec-1.9.jar).
6. Commons-Logging from [commons-logging-1.2.jar](http://repo1.maven.org/maven2/commons-logging/commons-logging/1.2/commons-logging-1.2.jar).
7. Google-Gson from [gson-2.7.jar](http://repo1.maven.org/maven2/com/google/code/gson/gson/2.7/gson-2.7.jar).
8. Log4j-Core from [log4j-core-2.6.2.jar](http://repo1.maven.org/maven2/org/apache/logging/log4j/log4j-core/2.6.2/log4j-core-2.6.2.jar).
9. Log4j-Api from [log4j-api-2.6.2.jar](http://repo1.maven.org/maven2/org/apache/logging/log4j/log4j-api/2.6.2/log4j-api-2.6.2.jar).
10. Junit from [junit-4.12.jar](http://repo1.maven.org/maven2/junit/junit/4.12/junit-4.12.jar) (only if you wish to execute the Test cases).

Usage
-----

Setup the SDK environment using the following code:

    JuspayEnvironment.withBaseUrl(JuspayEnvironment.PRODUCTION_BASE_URL).withApiKey(<your_api_key>);
    
    Following baseUrls are available for your usage:
        1. JuspayEnvironment.PRODUCTION_BASE_URL
        2. JuspayEnvironment.SANDBOX_BASE_URL

Test
----

To run all the test cases, simply issue the following command

    mvn test //inside your ${PROJECT_DIRECTORY}

Questions?
----------

Still have any questions? Feel free to mail us here - support@juspay.in.